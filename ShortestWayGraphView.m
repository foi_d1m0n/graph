//
//  ShortestWayGraphView.m
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "ShortestWayGraphView.h"
#import "VertexNode.h"

@interface ShortestWayGraphView()

@property (nonatomic, strong) NSMutableArray *nodes;

@end

@implementation ShortestWayGraphView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor brownColor];
    }
    return self;
}

- (void)clearGraph
{
    [super clearGraph];
    
    for (VertexNode *node in self.nodes)
    {
        [node.markView removeFromSuperview];
    }
}

- (void)drawGraphWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented
{
    [super drawGraphWithMatrix:matrix isOriented:isOriented];
    
    [self drawBondsLength];
    
    self.nodes = [NSMutableArray array];
    for (VertexView *vertex in self.vertexes)
    {
        VertexNode *node = [[VertexNode alloc] initWithVertex:vertex];
        [self.nodes addObject:node];
    }
    
    [self findShortestWays];
}

- (VertexNode *)nodeByVertex:(VertexView *)vertex
{
    for (VertexNode *node in self.nodes)
    {
        if ([node.vertex isEqual:vertex])
        {
            return node;
        }
    }
    return nil;
}

- (NSArray *)childNodesFromNode:(VertexNode *)node
{
    NSMutableArray *childNodes = [NSMutableArray array];
    
    for (BondView *bond in node.vertex.bonds)
    {
        VertexView *childVertex = [node.vertex connectedVertexWithBond:bond];
        VertexNode *childNode = [self nodeByVertex:childVertex];
        if (!childNode.isUsed)
        {
            [childNodes addObject:childNode];
        }
    }
    
    return childNodes;
}

- (NSInteger)bondLengthBetweenNodes:(NSArray *)nodes
{
    VertexNode *currentNode = [nodes objectAtIndex:0];
    VertexNode *connectedNode = [nodes lastObject];
    
    for (BondView *bond in currentNode.vertex.bonds)
    {
        if ([[currentNode.vertex connectedVertexWithBond:bond] isEqual:connectedNode.vertex])
            return bond.length;
    }
    
    return 0;
}

- (VertexNode *)findNextNodeFromNodes:(NSArray *)nodes
{
    int min = INT_MAX;
    VertexNode *resultNode = nil;
    
    for (int i = 0; i < nodes.count; i++)
    {
        VertexNode *node = [nodes objectAtIndex:i];
        if (node.mark < min) {
            min = node.mark;
            resultNode = node;
        }
    }
    
    return resultNode;
}

- (void)findShortestWays
{
    VertexNode *currentNode = [self.nodes objectAtIndex:0];
    VertexNode *previousNode = nil;
   
    currentNode.mark = 0;
    
    while (true)
    {
        NSArray *childNodes = [self childNodesFromNode:currentNode];
        
        if (!childNodes.count || !currentNode)
            break;
        
        for (VertexNode *childNode in childNodes)
        {
            int bondLength = [self bondLengthBetweenNodes:@[currentNode, childNode]];
            if (childNode.mark > currentNode.mark + bondLength)
            {
                childNode.mark = currentNode.mark + bondLength;
            }
        }
        
        currentNode.isUsed = YES;
        
        previousNode = currentNode;
        currentNode = [self findNextNodeFromNodes:childNodes];
        
        BondView *bond = [previousNode.vertex bondWhichConnectingVertexes:@[previousNode.vertex, currentNode.vertex]];
        bond.color = [UIColor redColor];
    }
        
}



@end
