//
//  MatrixView.m
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MatrixView.h"

#import "MatrixCell.h"

#import "TextView.h"

@interface MatrixView() <MatrixCellDelegate>

@end

@implementation MatrixView

static const float kLeftOffset = 40;
static const float kRightOffset = 40;
static const float kTopOffset = 40;
static const float kBottomOffset = 40;

- (void)setSize:(CGSize)size
{
    _size = size;
    [self refreshMatix];
}

-(NSMutableArray *)functionsHeaders
{
    if (!_functionsHeaders)
    {
        _functionsHeaders = [NSMutableArray array];
    }
    return _functionsHeaders;
}

- (NSMutableArray *)modulesHeaders
{
    if (!_modulesHeaders)
    {
        _modulesHeaders = [NSMutableArray array];
    }
    return _modulesHeaders;
}

- (void)reloadMatrix
{
    for (MatrixCell *cell in self.matrix)
    {
        cell.backgroundColor = [UIColor yellowColor];
    }
    
    for (TextView *moduleHeader in self.modulesHeaders)
    {
        moduleHeader.backgroundColor = [UIColor greenColor];
    }
}

- (void)refreshMatix
{
    [self clearMatrix];
    [self buildMatrix];
    [self showMatrix];
}

- (void)clearMatrix
{
    for (int i = 0; i < self.matrix.count; i++)
    {
        [(MatrixCell *)[self.matrix objectAtIndex:i] removeFromSuperview];
    }
    
    [self.matrix removeAllObjects];
    
    for (TextView *header in self.functionsHeaders)
         [header removeFromSuperview];
    
    [self.functionsHeaders removeAllObjects];
    
    for (TextView *header in self.modulesHeaders)
        [header removeFromSuperview];
    
    [self.modulesHeaders removeAllObjects];
    
}

- (void)showMatrix
{
    float cellWidth = [self cellWidth];
    float cellHeight = [self cellHeight];
    float xOffset = [self leftOffset];
    float yOffset = [self topOffset];
    
//    [self showModulesHeader];
//    [self showFunctionsHeader];

    for (MatrixCell *cell in self.matrix)
    {
        cell.frame = CGRectMake(xOffset + cell.column * cellWidth, yOffset + cell.row * cellHeight, cellWidth, cellHeight);
        [self addSubview:cell];
    }
}

- (void)showModulesHeader
{
    for (int i = 0; i < self.size.height; i++)
    {
        NSString *text = [NSString stringWithFormat:@"m%i", i];
        TextView *moduleHeader = [[TextView alloc] initWithFrame:CGRectMake(0, [self topOffset] + i * [self cellHeight], [self leftOffset], [self cellHeight]) text:text];
        [self addSubview:moduleHeader];
        [self.modulesHeaders addObject:moduleHeader];
    }
}

- (void)showFunctionsHeader
{
    for (int i = 0; i < self.size.width; i++)
    {
        NSString *text = [NSString stringWithFormat:@"f%i", i];
        TextView *functionHeader = [[TextView alloc] initWithFrame:CGRectMake([self leftOffset] + i * [self cellWidth], 0, [self cellWidth], [self topOffset]) text:text];
        [self addSubview:functionHeader];
        [self.functionsHeaders addObject:functionHeader];
    }
}

- (void)buildMatrix
{
    self.matrix = [NSMutableArray arrayWithCapacity:self.size.width * self.size.height];
    
    for (int i = 0; i < self.size.height * self.size.width; i++)
    {
        int row = (int)(i / self.size.width);
        int column = (int)(i % (int)(self.size.width));
        MatrixCell *cell = [[MatrixCell alloc] initWithRow:row column:column delegate:self];
        [self.matrix addObject:cell];
    }
}

- (void)generateRandomnMatrixWithPercentage:(float)percentage
{
    for (MatrixCell *cell in self.matrix) {
        if (cell.value) {
            cell.value = 0;
        }
    }
    
    int maxRibCount = self.size.width * self.size.height;
    
    int ribCount = (int)(maxRibCount * percentage / 100);
    
    for (int i = 0; i < ribCount; i++)
    {
        while (YES)
        {
            int row = (int)(rand() % (int)(self.size.height));
            int column = (int)(rand() % (int)(self.size.width));
            
            int index = row * self.size.width + column;
            if (index >= self.matrix.count)
            {
                NSLog(@"row %i, column %i, size:%@", row, column, NSStringFromCGSize(self.size));
            }
            MatrixCell *cell = [self.matrix objectAtIndex:index];
            
            if (cell.value)
                continue;
            
            else
            {
                cell.value = 1;
                break;
            }
        }
    }
}

- (float)leftOffset
{
    return kLeftOffset;
}

- (float)rightOffset
{
    return kRightOffset;
}

- (float)topOffset
{
    return kTopOffset;
}

- (float)bottomOffset
{
    return kBottomOffset;
}

- (float)cellWidth
{
    return ((self.frame.size.width - [self leftOffset] - [self rightOffset]) / self.size.width);
}

- (float)cellHeight
{
    return ((self.frame.size.height - [self topOffset] - [self bottomOffset]) / self.size.height);
}

#pragma mark - Matrix cell delegate

-(void)matrixCellDidDoubleTapped:(MatrixCell *)matrixCell
{
    
}

@end
