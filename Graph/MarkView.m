//
//  MarkView.m
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MarkView.h"

@implementation MarkView


- (id)initWithCenterPoint:(CGPoint)center mark:(NSInteger)mark;
{
    self = [super initWithFrame:CGRectMake(0, 0, kMarkViewSize, kMarkViewSize)];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.mark = mark;
        self.center = center;
    }
    return self;
}

- (void)setMark:(NSInteger)mark
{
    _mark = mark;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    NSString *markString = self.mark == INT_MAX ? [NSString stringWithFormat:@"∞"]:[NSString stringWithFormat:@"%i", self.mark];
//    [markString drawAtPoint:CGPointMake(self.frame.size.width / 3, self.frame.size.height / 3) withFont:[UIFont boldSystemFontOfSize:20]];
    [markString drawAtPoint:CGPointZero withFont:[UIFont boldSystemFontOfSize:20]];
}

@end
