//
//  BondView.h
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VertexView.h"
#import "LengthView.h"

@class VertexView;

@interface BondView : UIView

@property (nonatomic, strong) VertexView *firstVertex;
@property (nonatomic, strong) VertexView *secondVertex;

@property (nonatomic) BOOL duplex;
@property (nonatomic) NSInteger length;
@property (nonatomic, strong) LengthView *lengthView;
@property (nonatomic, strong) UIColor *color;

- (id)initWithStartVertex:(VertexView *)startVertex endVertex:(VertexView *)endVertex length:(NSInteger)length;

@end
