//
//  MazeEdge.h
//  Graph
//
//  Created by Dmitry Simkin on 4/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MatrixCell.h"

@interface MazeEdge : NSObject

@property (nonatomic, strong) MatrixCell *object;
@property (nonatomic) BOOL visited;

- (id)initWithCell:(MatrixCell *)cell;

@end
