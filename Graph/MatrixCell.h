//
//  MatrixCell.h
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef enum MatrixCellValue{
//    kMatrixCellValueZero = 0,
//    kMatrixCellValueOne = 1
//} MatrixCellValue;

@class MatrixCell;

@protocol MatrixCellDelegate

//method's name sucks
- (void)matrixCellDidDoubleTapped:(MatrixCell *)matrixCell;

@end

@interface MatrixCell : UIView

@property (nonatomic, weak) id <MatrixCellDelegate> delegate;

@property (nonatomic) NSInteger row;
@property (nonatomic) NSInteger column;

- (id)initWithRow:(NSInteger)row column:(NSInteger)column delegate:(id <MatrixCellDelegate>)delegate;

@property (nonatomic) NSInteger value;

@end

