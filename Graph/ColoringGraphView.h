//
//  ColoringGraphView.h
//  Graph
//
//  Created by Dmitry Simkin on 05.04.13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "GraphView.h"

@interface ColoringGraphView : GraphView

- (void)colorizeGraphWithMatrix:(NSArray *)matrix;

@end
