//
//  MatrixView.h
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatrixView : UIView

@property (nonatomic) CGSize size;

@property (nonatomic, strong) NSMutableArray *matrix;

@property (nonatomic, strong) NSMutableArray *modulesHeaders;
@property (nonatomic, strong) NSMutableArray *functionsHeaders;

- (void)generateRandomnMatrixWithPercentage:(float)percentage;
- (void)reloadMatrix;

@end
