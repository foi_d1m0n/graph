//
//  GraphViewController.m
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "GraphViewController.h"

#import "GraphView.h"
#import "ShortestWayGraphView.h"
#import "ColoringGraphView.h"

@interface GraphViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;

@property (nonatomic, strong) NSMutableArray *graphs;
@property (nonatomic, copy) NSArray *matrix;

@end

@implementation GraphViewController

- (GraphView *)graphByClass:(Class)class
{
    for (GraphView *view in self.graphs)
    {
        if ([view isKindOfClass:class])
            return view;
    }
    
    return nil;
}

- (GraphView *)graphView
{
    return [self graphByClass:[GraphView class]];;
}

- (ShortestWayGraphView *)shortestWayGraph
{
    return (ShortestWayGraphView *)[self graphByClass:[ShortestWayGraphView class]];
}

- (ColoringGraphView *)coloringGraphView
{
    return (ColoringGraphView *)[self graphByClass:[ColoringGraphView class]];
}

- (void)refreshGraphWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented
{
    self.matrix = matrix;
    [[self graphView] drawGraphWithMatrix:matrix isOriented:isOriented];
}

- (void)findShortestWayWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented
{
    self.matrix = matrix;
    if (![self shortestWayGraph])
    {
        ShortestWayGraphView *shortestWayGraph = [[ShortestWayGraphView alloc] initWithFrame:CGRectMake(0, [self yOffsetToNextGraphView], self.view.frame.size.width, 700)];
        [self.graphs addObject:shortestWayGraph];
        [self.containerScrollView addSubview:shortestWayGraph];
    }
    
    [self refreshScrollviewContentSize];
    
    [[self shortestWayGraph] drawGraphWithMatrix:matrix isOriented:YES];
    
    [self.containerScrollView scrollRectToVisible:[self shortestWayGraph].frame animated:YES];
}

- (float)yOffsetToNextGraphView
{
    float height = 0;
    
    for (GraphView *view in self.graphs)
        height += view.frame.size.height;
    
    return height;
}

- (void)refreshScrollviewContentSize
{
    float height = 0;
    
    for (GraphView *view in self.graphs)
        height += view.frame.size.height;
    
    self.containerScrollView.contentSize = CGSizeMake(self.view.frame.size.width, height);
}

- (void)matrixCellValueChanged:(MatrixCell *)cell
{
    if (cell.value)
        [self.graphView drawNwewBondWithMAtrixCell:cell];
    else
        [self.graphView deleteBondWithMatrixCell:cell];
}

#pragma mark - LAB3 Coloring

- (void)colorizeGraphWithMatrix:(NSArray *)matrix;
{
    if (![self coloringGraphView])
    {
        ColoringGraphView *coloringGraphView = [[ColoringGraphView alloc] initWithFrame:CGRectMake(0, [self yOffsetToNextGraphView], self.view.frame.size.width, 700)];
        [self.graphs addObject:coloringGraphView];
        [self refreshScrollviewContentSize];
        [self.containerScrollView addSubview:coloringGraphView];
    }
    
    [[self coloringGraphView] colorizeGraphWithMatrix:matrix];
}

#pragma mark - View controller lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.graphs = [NSMutableArray array];
    
    GraphView *graphView = [[GraphView alloc] initWithFrame:CGRectMake(0, [self yOffsetToNextGraphView], self.view.frame.size.width, 700)];
    [self.graphs addObject:graphView];
    [self.containerScrollView addSubview:graphView];
    [self refreshScrollviewContentSize];
}

@end
