//
//  LengthView.m
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "LengthView.h"

@implementation LengthView

- (id)initWithCenterPoint:(CGPoint)center length:(NSInteger)length;
{
    self = [super initWithFrame:CGRectMake(0, 0, kLengthViewSize, kLengthViewSize)];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.lengh = length;
        self.center = center;
    }
    return self;
}

- (void)setLengh:(NSInteger)lengh
{
    _lengh = lengh;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    NSString *lengthString = [NSString stringWithFormat:@"%i", self.lengh];
    [lengthString drawAtPoint:CGPointZero withFont:[UIFont boldSystemFontOfSize:20]];
}

@end
