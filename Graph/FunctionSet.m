//
//  FunctionSet.m
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "FunctionSet.h"

@implementation FunctionSet

-(id)initWithNumber:(NSInteger)number
{
    self = [super init];
    if (self)
    {
        self.modules = [NSMutableArray array];
        self.number = number;
    }
    return self;
}

- (int)modulesCount
{
    int count = 0;
    for (NSNumber *modules in self.modules)
    {
        if (modules.intValue)
            count++;
    }
    return count;
}

- (int)firstModuleIndex
{
    for (int i = 0; i < self.modules.count; i++)
    {
        NSNumber *module = [self.modules objectAtIndex:i];
        if (module.intValue)
            return i;
    }
    return -1;
}

@end
