//
//  BondView.m
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "BondView.h"
#import "GraphView.h"

@interface BondView()

@property (nonatomic) CGPoint startPoint;
@property (nonatomic) CGPoint endPoint;

@property (nonatomic, strong) UIPopoverController *popover;

@end

@implementation BondView

@synthesize duplex = _duplex;

static const int kBondWidth = 10;

- (id)initWithStartVertex:(VertexView *)startVertex endVertex:(VertexView *)endVertex length:(NSInteger)length;{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.color = [UIColor greenColor];
        
        self.firstVertex = startVertex;
        self.secondVertex = endVertex;
        self.startPoint = startVertex.center;
        self.endPoint = endVertex.center;
        self.length = length;
        
        float xDiff = self.endPoint.x - self.startPoint.x;
        float yDiff = self.endPoint.y - self.startPoint.y;
        float xy = sqrtf(powf(xDiff, 2) + powf(yDiff, 2));

        self.bounds = CGRectMake(0, 0, xy, kBondWidth);
        self.center = CGPointMake((self.startPoint.x + self.endPoint.x) / 2, (self.startPoint.y + self.endPoint.y) / 2);
        
        float cos = xDiff / xy;
        float sin = yDiff / xy;
                    
        float angleRad = asinf(sin);
        int k = cos < 0 ? -1 : 1;

        self.transform = CGAffineTransformMakeRotation(k * angleRad);
    }
    return self;
}

- (void)setLength:(NSInteger)length
{
    _length = length;
    self.lengthView.lengh = length;
}

- (BOOL)duplex
{
    return NO;
}

- (void)setColor:(UIColor *)color
{
    _color = color;
    [self setNeedsDisplay];
}

- (void)setDuplex:(BOOL)isDuplex
{
    _duplex = isDuplex;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextBeginPath(context);
    CGContextSetLineWidth(context, kBondWidth / 4);
    CGContextSetStrokeColor(context, CGColorGetComponents([self.color CGColor]));
    CGContextMoveToPoint(context, 0, self.bounds.size.height / 2);
    CGContextAddLineToPoint(context, self.bounds.size.width, self.bounds.size.height / 2);
    CGContextDrawPath(context, kCGPathStroke);
}

@end
