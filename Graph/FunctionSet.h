//
//  FunctionSet.h
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FunctionSet : NSObject

@property (nonatomic, strong) NSMutableArray *modules;
@property (nonatomic) NSInteger number;
-(id)initWithNumber:(NSInteger)number;
- (int)modulesCount;
- (int)firstModuleIndex;

//- (void)removeFunctionAtIndexes:(NSIndexSet *)indexSet;

@end
