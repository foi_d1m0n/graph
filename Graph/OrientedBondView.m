//
//  OrientedBondView.m
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "OrientedBondView.h"

@interface OrientedBondView()

@property (nonatomic) NSInteger quarter;
//@property (nonatomic) BOOL duplex;

@end

@implementation OrientedBondView

@synthesize duplex = _duplex;

static const int kArrowXOffset = 30;

- (id)initWithStartVertex:(VertexView *)startVertex endVertex:(VertexView *)endVertex length:(NSInteger)length;
{
    self = [super initWithStartVertex:startVertex endVertex:endVertex length:length];
    if (self)
    {
        float xDiff = endVertex.center.x - startVertex.center.x;
        float yDiff = endVertex.center.y - startVertex.center.y;
        float xy = sqrtf(powf(xDiff, 2) + powf(yDiff, 2));
            
        float cos = xDiff / xy;
        float sin = yDiff / xy;
        
        if (cos >= 0)
        {
            if (sin >= 0)
                self.quarter = 4;
            else
                self.quarter = 1;
        }
        else
        {
            if (sin > 0)
                self.quarter = 3;
            else
                self.quarter = 2;
        }
    }
    return self;
}

- (BOOL)duplex
{
    return _duplex;
}
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextBeginPath(context);
    
    if (self.duplex)
    {
        [self drawStartArraw:context];
        [self drawEndArraw:context];
    }
    else
    {
        if (self.quarter == 1 || self.quarter == 4)
            [self drawStartArraw:context];
        else
            [self drawEndArraw:context];
    }
    
    CGContextDrawPath(context, kCGPathStroke);
}

- (void)drawStartArraw:(CGContextRef)context
{
    CGContextMoveToPoint(context, self.bounds.size.width, self.bounds.size.height / 2);
    CGContextAddLineToPoint(context, self.bounds.size.width - kArrowXOffset, 0);
    CGContextMoveToPoint(context, self.bounds.size.width, self.bounds.size.height / 2);
    CGContextAddLineToPoint(context, self.bounds.size.width - kArrowXOffset, self.bounds.size.height);
}

- (void)drawEndArraw:(CGContextRef)context
{
    CGContextMoveToPoint(context, 0, self.bounds.size.height / 2);
    CGContextAddLineToPoint(context, kArrowXOffset, 0);
    CGContextMoveToPoint(context, 0, self.bounds.size.height / 2);
    CGContextAddLineToPoint(context, kArrowXOffset, self.bounds.size.height);
}


@end
