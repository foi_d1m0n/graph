//
//  GraphView.m
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "GraphView.h"
#import "VertexView.h"
#import "BondView.h"
#import "OrientedBondView.h"
#import "MatrixCell.h"
#import "LengthView.h"

@interface GraphView()

@property (nonatomic, strong) NSArray *matrix;
@property (nonatomic) BOOL isOriented;

@property (nonatomic) NSInteger vertexesCount;

@end

@implementation GraphView

static const int kBordersSize = 100;
static const int kVertexDiameter = 40;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor grayColor];
    }
    return self;
}

- (void)drawGraphWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented
{
    [self clearGraph];
    
    self.matrix = matrix;
    self.isOriented = isOriented;
    
    [self drawGraph];
}

- (void)drawNwewBondWithMAtrixCell:(MatrixCell *)cell
{
    [self drawBondByMatrixCell:cell];
}

- (void)deleteBondWithMatrixCell:(MatrixCell *)cell
{
    VertexView *startVertex = [self.vertexes objectAtIndex:cell.row];
    VertexView *endVertex = [self.vertexes objectAtIndex:cell.column];
    
    BondView *bond = [startVertex isBondedWithVertex:endVertex];
    [startVertex deleteBond:bond];
    [endVertex deleteBond:bond];
    [self.bonds removeObject:bond];
    [bond removeFromSuperview];
    
    if (bond.duplex)
    {
        [self drawBondByMatrixCell:cell];
    }
}

- (void)clearGraph
{
    for (VertexView *vertex in self.vertexes)
        [vertex removeFromSuperview];
    
    for (BondView *bond in self.bonds)
    {
        [bond removeFromSuperview];
        [bond.lengthView removeFromSuperview];
    }
    
    self.vertexes = nil;
    self.bonds = nil;
}

- (void)drawGraph
{
    [self drawVertexes];
    [self drawBonds];
}

- (void)drawBonds   
{
    self.bonds = [NSMutableArray array];

    for (MatrixCell *cell in self.matrix)
    {
        if (cell.value)
        {
            [self drawBondByMatrixCell:cell];
        }
    }
}

- (void)drawBondsLength
{
    for (BondView *bond in self.bonds)
    {
        bond.lengthView = [[LengthView alloc] initWithCenterPoint:bond.center length:bond.length];
        [bond.superview addSubview:bond.lengthView];
    }
}

- (void)drawBondByMatrixCell:(MatrixCell *)cell
{
    VertexView *startVetex = (VertexView *)[self.vertexes objectAtIndex:cell.row];
    VertexView *endVertex = (VertexView *)[self.vertexes objectAtIndex:cell.column];
    
    BondView *bond;
    if (!self.isOriented)
    {
        OrientedBondView *orientedBond = (OrientedBondView *)[startVetex isBondedWithVertex:endVertex];
        if (!orientedBond)
        {
            orientedBond = [[OrientedBondView alloc] initWithStartVertex:startVetex endVertex:endVertex length:cell.value];
            [startVetex addBond:orientedBond];
            [endVertex addBond:orientedBond];
            [self.bonds addObject:orientedBond];
            [self addSubview:orientedBond];
        }
        else
        {
            orientedBond.duplex = YES;
        }
        bond = orientedBond;
        
    }
    else
    {
        bond = [[BondView alloc] initWithStartVertex:startVetex endVertex:endVertex length:cell.value];
        [startVetex addBond:bond];
        [endVertex addBond:bond];
        [self.bonds addObject:bond];
        [self addSubview:bond];
    }
}

- (void)drawVertexes
{
    self.vertexesCount = (NSInteger)sqrt(self.matrix.count);
    self.vertexes = [NSMutableArray arrayWithCapacity:self.vertexesCount];
    
    NSInteger radius = (MIN(self.frame.size.width, self.frame.size.height) - kBordersSize) / 2;
    
    double angle = 2 * M_PI / self.vertexesCount;
    
    for (int i = 0; i < self.vertexesCount; i++)
    {
        double xOffset = radius * cos(i * angle);
        double yOffset = radius * sin(i * angle);
        
        if (self.center.y > self.frame.size.height)
            self.innerCenter = CGPointMake(self.center.x, self.center.y - self.frame.origin.y);
        else
            self.innerCenter = self.center;
        
        VertexView *vertex = [[VertexView alloc] initWithOrigin:CGPointMake(self.innerCenter.x - xOffset, self.innerCenter.y - yOffset) diamenter:kVertexDiameter vertexNumber:i + 1];
        [self addSubview:vertex];
        [vertex setNeedsDisplay];
        [self.vertexes addObject:vertex];
    }
}

@end
