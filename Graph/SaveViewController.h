//
//  SaveViewController.h
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SaveViewController;

@protocol SaveViewControllerDelegate <NSObject>

- (void)saveViewController:(SaveViewController *)saveViewController didEnteredRecordName:(NSString *)name;

@end


@interface SaveViewController : UIViewController

@property (nonatomic, weak) id <SaveViewControllerDelegate> delegate;

@end
