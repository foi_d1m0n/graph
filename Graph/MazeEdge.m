//
//  MazeEdge.m
//  Graph
//
//  Created by Dmitry Simkin on 4/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MazeEdge.h"

@implementation MazeEdge

- (id)initWithCell:(MatrixCell *)cell
{
    self = [super init];
    if (self)
    {
        self.object = cell;
    }
    return self;
}

@end
