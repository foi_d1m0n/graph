//
//  Module.h
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Module : NSObject

@property (nonatomic, strong) NSMutableArray *functions;
@property (nonatomic) NSInteger number;
- (id)initWithNumber:(NSInteger)number;
- (int)functionsCount;
- (int)firstFunctionIndex;
- (NSIndexSet *)functionIndexes;

- (void)removeFunctionAtIndexes:(NSIndexSet *)indexSet;

@end
