//
//  MarkView.h
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

static const int kMarkViewSize = 20;

@interface MarkView : UIView

@property (nonatomic) NSInteger mark;

- (id)initWithCenterPoint:(CGPoint)center mark:(NSInteger)mark;

@end
