//
//  VertexView.h
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BondView.h"

@class BondView;

@interface VertexView : UIView

@property (nonatomic) NSInteger vertexNumber;
@property (nonatomic, strong) NSMutableArray *bonds;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic) BOOL isColorized;

- (id)initWithOrigin:(CGPoint)origin diamenter:(NSInteger)diamenter vertexNumber:(NSInteger)vertexNumber;
- (void)addBond:(BondView *)bond;
- (void)deleteBond:(BondView *)bond;
- (BondView *)isBondedWithVertex:(VertexView *)vertex;

- (VertexView *)connectedVertexWithBond:(BondView *)bond;
- (BOOL)isBondedWithVertex:(VertexView *)vertex andColor:(UIColor *)color;
- (BOOL)isBondedWithChildVertexesWithColor:(UIColor *)color;

- (BondView *)bondWhichConnectingVertexes:(NSArray *)vertexes;

- (NSArray *)connectedVertexes;

- (NSInteger)diameter;

@end
