//
//  ConfigurationViewController.m
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "ConfigurationViewController.h"
#import "GraphViewController.h"
#import "SaveViewController.h"
#import "MatrixListViewController.h"
#import "MatrixCell.h"

#define RECORDS @"records"
#define RECORD_NAME @"recordName"
#define MATRIX @"matrix"
#define ROW @"row"
#define COLUMN @"column"
#define VALUE @"value"
#define ORIENTED @"isOriented"

@interface ConfigurationViewController () <UIPickerViewDataSource, UIPickerViewDelegate, MatrixCellDelegate, SaveViewControllerDelegate, MatrixListViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *matrixContainerView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *orientedSegmentedControl;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;

@property (weak, nonatomic) IBOutlet GraphViewController *graphViewController;

@property (nonatomic) BOOL isOriented;

@property (nonatomic, strong) NSArray *vertexCountValues;
@property (nonatomic) NSUInteger selectedVertexCount;
@property (nonatomic) NSUInteger vertexCount;

@property (nonatomic, strong) NSArray *percentages;

@property (nonatomic, strong) NSMutableArray *matrix;

@end

@implementation ConfigurationViewController

static const int kMaxVertexCount = 10;
static const int kMinVertexCount = 4;
static const int kDefaultVertexCount = 6;
static const int kMatrixCellWidth = 32;
static const int kMatrixCellHeight = 32;
static const int kMatrixBorderSize = 20;

- (void)refreshMatrix
{
    self.isOriented = self.orientedSegmentedControl.selectedSegmentIndex == 0 ? NO : YES;
    self.vertexCount = self.selectedVertexCount;
    [self clearMatrix];
    [self buildMatrix];
    [self showMatrix];
    
    [self.graphViewController refreshGraphWithMatrix:self.matrix isOriented:self.isOriented];
}

- (void)clearMatrix
{
    for (int i = 0; i < self.matrix.count; i++)
    {
        [(MatrixCell *)[self.matrix objectAtIndex:i] removeFromSuperview];
    }
    
    [self.matrix removeAllObjects];
}

- (void)showMatrix
{
    int rowOffsetForOrientedGraph = 0;
    for (int i = 0; i < self.vertexCount; i++)
    {
        for (int j = rowOffsetForOrientedGraph; j < self.vertexCount; j++)
        {
            MatrixCell *cell = [self.matrix objectAtIndex: i * self.vertexCount + j];
            cell.frame = CGRectMake(j * kMatrixCellWidth, i * kMatrixCellHeight, kMatrixCellWidth, kMatrixCellHeight);
            [self.matrixContainerView addSubview:cell];
            if (i == j)
            {
                cell.backgroundColor = [UIColor grayColor];
                cell.userInteractionEnabled = NO;
            }
        }
        if (self.isOriented)
            rowOffsetForOrientedGraph ++;
    }
}

- (void)buildMatrix
{
    self.matrix = [NSMutableArray arrayWithCapacity:self.selectedVertexCount * self.selectedVertexCount];
    
    for (int i = 0; i < self.selectedVertexCount * self.selectedVertexCount; i++)
    {
        MatrixCell *cell = [[MatrixCell alloc] initWithRow:(int)(i / self.selectedVertexCount) column:(int)(i % self.selectedVertexCount) delegate:self];
        [self.matrix addObject:cell];
    }
}
- (void)orientedSegmentedControlValueChanged:(id)segmentedControl
{
    [self refreshMatrix];
}

- (void)matrixCellDidDoubleTapped:(MatrixCell *)matrixCell
{
    if (matrixCell.value)
    {
        MatrixCell *oppositeCell = [self.matrix objectAtIndex:matrixCell.column * self.selectedVertexCount + matrixCell.row];
        
        if (oppositeCell.value)
            oppositeCell.value = 0;
    }
    
    [self.graphViewController refreshGraphWithMatrix:self.matrix isOriented:self.isOriented];
}

- (IBAction)colorizeButtonPressed:(id)sender
{
    [self.graphViewController colorizeGraphWithMatrix:self.matrix];
}

- (IBAction)shortestWayWasTapped:(id)sender
{
//    NSArray *rows = @[@"0",@"0",@"1",@"1",@"2",@"2",@"3",@"3",@"4"];
//    NSArray *columns = @[@"1",@"2",@"2",@"3",@"3",@"4",@"4",@"5",@"5"];
//    NSArray *values = @[@"10",@"2",@"3",@"7",@"12",@"7",@"9",@"1",@"6"];
//    
//    NSMutableArray *matrix = [NSMutableArray array];
//    for (int i = 0; i < values.count; i++)
//    {
//        NSString *rowString = (NSString *)[rows objectAtIndex:i];
//        NSString *columnString = (NSString *)[columns objectAtIndex:i];
//        NSString *valueString = (NSString *)[values objectAtIndex:i];
//        
//        MatrixCell *cell = [[MatrixCell alloc] initWithRow:rowString.intValue column:columnString.intValue delegate:nil];
//        cell.value = valueString.intValue;
//        [matrix addObject:cell];
//    }
//    
//    for (int i = values.count; i < self.selectedVertexCount * self.selectedVertexCount; i++)
//    {
//        MatrixCell *cell = [[MatrixCell alloc] initWithRow:0 column:0 delegate:nil];
//        [matrix addObject:cell];
//    }
    
    [self.graphViewController findShortestWayWithMatrix:self.matrix isOriented:self.isOriented];
}

- (IBAction)generateGraph:(id)sender
{
    for (MatrixCell *cell in self.matrix) {
        if (cell.value) {
            cell.value = 0;
        }
    }
    
    NSNumber *percent = [self.percentages objectAtIndex:[self.picker selectedRowInComponent:1]];
    
    int maxRibCount = self.selectedVertexCount * (self.selectedVertexCount - 1) / 2;
    
    int ribCount = (int)(maxRibCount * percent.intValue / 100);
    
    for (int i = 0; i < ribCount; i++)
    {
        while (YES)
        {
            int row = (int)(rand() % self.selectedVertexCount);
            int column = (int)(rand() % self.selectedVertexCount);
            
            if (row == column)
                continue;
            
            MatrixCell *cell = [self.matrix objectAtIndex:row * self.selectedVertexCount + column];
            MatrixCell *oppositeCell = [self.matrix objectAtIndex:column * self.selectedVertexCount + row];
            
            if (!self.isOriented)
            {
                if (cell.value || oppositeCell.value)
                    continue;
                else
                {
                    cell.value = 1;
                    break;
                }
            }
            else
            {
                if (cell.value || cell.row > cell.column)
                    continue;
                else
                {
                    cell.value = 1;
                    break;
                }
            }
            
        }
    }
    
    [self.graphViewController refreshGraphWithMatrix:self.matrix isOriented:self.isOriented];
}

- (NSArray *)recordNames
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *records = [defaults arrayForKey:RECORDS];
    NSMutableArray *recordNames = [NSMutableArray array];
    
    for (NSDictionary *record in records)
        [recordNames addObject:[record objectForKey:RECORD_NAME]];
    
    return recordNames;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowSaveVC"])
    {
        SaveViewController *saveVC = segue.destinationViewController;
        saveVC.delegate = self;
    }
    if ([segue.identifier isEqualToString:@"ShowMatrixList"])
    {
        MatrixListViewController *matrixListVC = segue.destinationViewController;
        matrixListVC.delegate = self;
        matrixListVC.recordNames = [self recordNames];
    }
}

- (void)saveMatrixWithRecordName:(NSString *)name
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *records = [[defaults arrayForKey:RECORDS] mutableCopy];
    
    if (!records)
        records = [NSMutableArray array];
    
    NSMutableArray *matrix = [NSMutableArray array];
    
    for (MatrixCell *cell in self.matrix)
    {
        NSDictionary *cellDict = @{ROW : [NSNumber numberWithInt:cell.row],
        COLUMN  : [NSNumber numberWithInt:cell.column],
        VALUE : [NSNumber numberWithInt:cell.value]};
        [matrix addObject:cellDict];
    }
    
    NSDictionary *record = @{MATRIX : matrix, RECORD_NAME : name, ORIENTED : [NSNumber numberWithBool:self.isOriented]};
    
    [records addObject:record];
    [defaults setObject:records forKey:RECORDS];
    [defaults synchronize];
    
}

- (void)saveViewController:(SaveViewController *)saveViewController didEnteredRecordName:(NSString *)name
{
    [self saveMatrixWithRecordName:name];
}

- (void)matrixListViewController:(MatrixListViewController *)matrixListViewController didSelectRecordWithName:(NSString *)name
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *records = [defaults arrayForKey:RECORDS];
    
    for (NSDictionary *record in records)
    {
        NSString *recordName = [record objectForKey:RECORD_NAME];
        if ([recordName isEqualToString:name])
        {
            NSArray *matrix = [record objectForKey:MATRIX];
            BOOL isOriented = [[record objectForKey:ORIENTED] boolValue];
            
            if (isOriented)
                self.orientedSegmentedControl.selectedSegmentIndex = 1;
            else
                self.orientedSegmentedControl.selectedSegmentIndex = 0;
            self.isOriented = isOriented;
            
            self.vertexCount = (int)sqrt(matrix.count);
            
            [self clearMatrix];
            
            for (NSDictionary *cellDict in matrix)
            {
                int row = [[cellDict objectForKey:ROW] intValue];
                int column = [[cellDict objectForKey:COLUMN] intValue];
                int value = [[cellDict objectForKey:VALUE] intValue];
                
                MatrixCell *cell = [[MatrixCell alloc] initWithRow:row column:column delegate:self];
                cell.value = value;
                [self.matrix addObject:cell];
            }
            
            [self showMatrix];
            
            [self.graphViewController refreshGraphWithMatrix:self.matrix isOriented:self.isOriented];
            break;
        }
    }
}

#pragma mark - Picker

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return self.picker.frame.size.width / 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (!component)
        return self.vertexCountValues.count;
    else
        return self.percentages.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (!component)
        return [self.vertexCountValues objectAtIndex:row];
    else
    {
        NSNumber *percent = [self.percentages objectAtIndex:row];
        return [NSString stringWithFormat:@"%i%%", percent.intValue];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (!component)
    {
        NSString *countString = [self.vertexCountValues objectAtIndex:row];
        self.selectedVertexCount = countString.integerValue;
        [self refreshMatrix];
    }
}

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.graphViewController = [self.splitViewController.viewControllers lastObject];
    
    NSMutableArray *vertexCounts = [NSMutableArray array];
    for (int i = kMinVertexCount; i <= kMaxVertexCount; i++)
    {
        [vertexCounts addObject:[NSString stringWithFormat:@"%i", i]];
    }
    self.vertexCountValues = vertexCounts;
    
    NSMutableArray *percentages = [NSMutableArray array];
    
    for (int i = 5; i <= 100; i += 5)
    {
        [percentages addObject:[NSNumber numberWithInt:i]];
    }
    self.percentages = percentages;    
    
    [self.picker selectRow:(kDefaultVertexCount - kMinVertexCount) inComponent:0 animated:NO];
    self.selectedVertexCount = kDefaultVertexCount;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.orientedSegmentedControl addTarget:self action:@selector(orientedSegmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self performSelectorOnMainThread:@selector(refreshMatrix) withObject:nil waitUntilDone:NO];
}

@end
