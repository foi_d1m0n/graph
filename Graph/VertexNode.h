//
//  VertexNode.h
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VertexView.h"
#import "MarkView.h"

@interface VertexNode : NSObject

@property (nonatomic, strong) VertexView *vertex;
@property (nonatomic) NSInteger mark;
@property (nonatomic, strong) MarkView *markView;
@property (nonatomic) BOOL isUsed;

- (id)initWithVertex:(VertexView *)vertex;

@end
