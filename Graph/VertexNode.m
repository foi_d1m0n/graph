//
//  VertexNode.m
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "VertexNode.h"
#import "GraphView.h"

@implementation VertexNode

- (id)initWithVertex:(VertexView *)vertex
{
    self = [super init];
    if (self)
    {
        self.vertex = vertex;
        self.mark = INT_MAX;
        
        GraphView *graph = (GraphView *)self.vertex.superview;
        
        CGPoint markCenterPoint;
        
        if (graph.innerCenter.x > self.vertex.center.x)
        {
            if (graph.innerCenter.y > self.vertex.center.y)
                markCenterPoint = CGPointMake(self.vertex.center.x - kMarkViewSize, self.vertex.center.y - kMarkViewSize);
            else
                markCenterPoint = CGPointMake(self.vertex.center.x - kMarkViewSize, self.vertex.center.y + kMarkViewSize);
        }
        else
        {
            if (graph.innerCenter.y > self.vertex.center.y)
                markCenterPoint = CGPointMake(self.vertex.center.x + kMarkViewSize, self.vertex.center.y - kMarkViewSize);
            else
                markCenterPoint = CGPointMake(self.vertex.center.x + kMarkViewSize, self.vertex.center.y + kMarkViewSize);
        }
        
        self.markView = [[MarkView alloc] initWithCenterPoint:markCenterPoint mark:self.mark];
        [graph addSubview:self.markView];
    }
    return self;
}

- (void)setMark:(NSInteger)mark
{
    _mark = mark;
    self.markView.mark = mark;
}

@end
