//
//  MinamalCoverViewController.m
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MinimalCoverViewController.h"

#import "MatrixView.h"
#import "MatrixCell.h"
#import "TextView.h"

#import "Module.h"
#import "FunctionSet.h"

typedef enum
{
    PickerComponentRows = 2,
    PickerComponentColumns = 0
} PickerComponentNumber;

@interface MinimalCoverViewController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet MatrixView *matrixContainerView;
@property (weak, nonatomic) IBOutlet UIPickerView *matrixSizePicker;

@property (nonatomic, strong) NSMutableDictionary *minCover;
@property (nonatomic, strong) NSMutableArray *covers;


@property (nonatomic, strong) NSArray *sizes;

@end

@implementation MinimalCoverViewController

static const int kMinMatrixSize = 4;
static const int kMaxMatrixSize = 15;
static const int kMatrixDefaultSize = 8;

static const int kMiddlePickerComponentWidth = 40;

- (void)awakeFromNib
{
    NSMutableArray *sizes = [NSMutableArray array];
    for (int i = kMinMatrixSize; i < kMaxMatrixSize; i++)
    {
        [sizes addObject:[NSString stringWithFormat:@"%i", i]];
    }
    self.sizes = [sizes copy];
}

- (IBAction)generateButtonPressed:(id)sender
{
    [self.matrixContainerView generateRandomnMatrixWithPercentage:35];
}

- (IBAction)calculateButtonPressed:(id)sender
{
    [self.matrixContainerView reloadMatrix];
//    NSDictionary *cover = [self findMinimalCoverFromMatrix:self.matrixContainerView.matrix];
    NSDictionary *cover = [self findMinimalCoverByOptimalRecursiveAlgorithmFromMatrix:self.matrixContainerView.matrix];
    [self showCover:cover];
}

- (void)showCover:(NSDictionary *)cover
{
    NSArray *matrix = self.matrixContainerView.matrix;
    
    for (int i = 0; i < self.matrixContainerView.size.width; i++)
    {
        NSString *key = [NSString stringWithFormat:@"%i", i];
        NSInteger moduleNumber = [[cover objectForKey:key] intValue];
        TextView *module = [self.matrixContainerView.modulesHeaders objectAtIndex:moduleNumber];
        module.backgroundColor = [UIColor blueColor];
        MatrixCell *cell = [matrix objectAtIndex:moduleNumber * self.matrixContainerView.size.width + i];
        cell.backgroundColor = [UIColor redColor];
    }
}

- (CGSize)size
{
    int rowsCount = [[self.sizes objectAtIndex:[self.matrixSizePicker selectedRowInComponent:PickerComponentRows]] intValue];
    int columnsCount = [[self.sizes objectAtIndex:[self.matrixSizePicker selectedRowInComponent:PickerComponentColumns]] intValue];

    return CGSizeMake(columnsCount, rowsCount);
}

- (NSDictionary *)findMinimalCoverByOptimalRecursiveAlgorithmFromMatrix:(NSArray *)theMatrix
{
    int rowsCount = (int)self.matrixContainerView.size.height;
    int columnsCount = (int)self.matrixContainerView.size.width;
    //    int matrix[rowsCount][columnsCount];
    
    NSMutableArray *modules = [NSMutableArray array];
    for (int i = 0; i < rowsCount; i++)
    {
        Module *module = [[Module alloc] initWithNumber:i];
        NSRange range = NSMakeRange(i * columnsCount, columnsCount);
        NSArray *row = [theMatrix subarrayWithRange:range];
        
        for (MatrixCell *cell in row)
            [module.functions addObject:[NSNumber numberWithInt:cell.value]];
        
        [modules addObject:module];
    }
    
    self.minCover = [NSMutableDictionary dictionary];
    self.covers = [NSMutableArray array];
    
    [self minCoverFromCover:[self.minCover mutableCopy] andModules:[modules mutableCopy]];

    return self.minCover;
}

- (void)minCoverFromCover:(NSMutableDictionary *)cover andModules:(NSMutableArray *)modules
{
    
}

- (NSDictionary *)findMinimalCoverFromMatrix:(NSArray *)theMatrix
{
    int rowsCount = (int)self.matrixContainerView.size.height;
    int columnsCount = (int)self.matrixContainerView.size.width;
//    int matrix[rowsCount][columnsCount];
    
    NSMutableArray *modules = [NSMutableArray array];
    for (int i = 0; i < rowsCount; i++)
    {
        Module *module = [[Module alloc] initWithNumber:i];
        NSRange range = NSMakeRange(i * columnsCount, columnsCount);
        NSArray *row = [theMatrix subarrayWithRange:range];
        
        for (MatrixCell *cell in row)
            [module.functions addObject:[NSNumber numberWithInt:cell.value]];
        
        [modules addObject:module];
    }
    
//    NSArray *originalModules = [modules copy];
    
    int coverCount = self.matrixContainerView.size.width;
    
    NSMutableDictionary *coveredFunctions = [NSMutableDictionary dictionary];
    NSMutableArray *minimalCoverModules = [NSMutableArray array];

    while (coveredFunctions.count < coverCount)
    {
        int maxValue = 0;
        Module *maxModule = nil;
        for (Module *module in modules)
        {
            int functionsCount = [module functionsCount];
            if (functionsCount > maxValue)
            {
                maxValue = functionsCount;
                maxModule = module;
            }
        }
        
        coveredFunctions = [self unionFunctions:coveredFunctions withNewFromModule:maxModule];
        NSIndexSet *indexSet = [maxModule functionIndexes];
        [minimalCoverModules addObject:maxModule];
        [modules removeObject:maxModule];
        
        for (Module *module in modules)
        {
            [module removeFunctionAtIndexes:indexSet];
        }
    }
        
    NSString *minimalCoverModulesString = [NSString string];
    for (Module *module in minimalCoverModules)
    {
        minimalCoverModulesString = [minimalCoverModulesString stringByAppendingFormat:@"%i ", module.number];
    }
    
    NSLog(@"%@", coveredFunctions.description);
    return coveredFunctions;
}

- (NSMutableDictionary *)unionFunctions:(NSDictionary *)covered withNewFromModule:(Module *)module
{
    NSMutableDictionary *result = [covered mutableCopy];
    for (int i = 0; i < module.functions.count; i++)
    {
        NSNumber *function = [module.functions objectAtIndex:i];
        if (function.intValue)
        {
            NSString *key = [NSString stringWithFormat:@"%i", i];
            Module *mod = [result objectForKey:key];
            if (!mod)
            {
                [result setValue:[NSNumber numberWithInt:module.number] forKey:key];
            }   
        }
    }
    return result;
}

- (NSMutableArray *)removeFunctionWithNumber:(int)number fromMatrix:(NSMutableArray *)matrix
{
    for (Module *module in matrix)
    {
        [module.functions removeObjectAtIndex:number];
    }
    return matrix;
}

#pragma mark - Picker

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == PickerComponentRows || component == PickerComponentColumns)
        return self.sizes.count;
    else
        return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == PickerComponentRows || component == PickerComponentColumns)
        return [self.sizes objectAtIndex:row];
    else
        return @"x";
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == PickerComponentRows || component == PickerComponentColumns)
        return (pickerView.frame.size.width - kMiddlePickerComponentWidth) / 2;
    else
        return kMiddlePickerComponentWidth;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.matrixContainerView.size = [self size];
}

#pragma mark - View controller lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.matrixSizePicker selectRow:(kMatrixDefaultSize - kMinMatrixSize) inComponent:PickerComponentRows animated:NO];
    [self.matrixSizePicker selectRow:(kMatrixDefaultSize - kMinMatrixSize) inComponent:PickerComponentColumns animated:NO];
    
    self.matrixContainerView.size = [self size];
}

@end
