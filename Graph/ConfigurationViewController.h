//
//  ConfigurationViewController.h
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConfigurationViewController;

@protocol ConfigurationViewControllerDelegate <NSObject>

- (void)configurationViewController:(ConfigurationViewController *)configurationViewController didSetNeedToRefreshGraphWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented;

@end

@interface ConfigurationViewController : UIViewController

@property (nonatomic, weak) id <ConfigurationViewControllerDelegate> delegate;

@end
