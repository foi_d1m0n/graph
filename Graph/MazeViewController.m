//
//  MazeViewController.m
//  Graph
//
//  Created by Dmitry Simkin on 4/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MazeViewController.h"

#import "MatrixView.h"
#import "MatrixCell.h"

#import "MazeEdge.h"

@interface MazeViewController ()

@property (weak, nonatomic) IBOutlet MatrixView *mazeView;
@property (nonatomic, strong) NSMutableArray *maze;

@end

@implementation MazeViewController

- (IBAction)generateButtonPressed:(id)sender
{
    [self generateMaze];
}

- (IBAction)clearButtonPressed:(id)sender
{
    [self generateMaze];
}

- (IBAction)findPathButtonPressed:(id)sender
{
    
}

- (void)generateMaze
{
    NSArray *matrix = self.mazeView.matrix;
    [self makeAllWalls:matrix];
    
    NSMutableArray *maze = [NSMutableArray arrayWithCapacity:matrix.count];
    
    for (MatrixCell *cell in matrix)
    {
        MazeEdge *edge = [[MazeEdge alloc] initWithCell:cell];
        [maze addObject:edge];
    }
    self.maze = maze;
    
    [self generateMazeFromEdge:[maze objectAtIndex:0]];
}

- (void)generateMazeFromEdge:(MazeEdge *)currentEdge
{
    currentEdge.visited = YES;
    MazeEdge *nextEdge = nil;
    NSArray *naighbors = [self naighborsFromEdge:currentEdge];
    
    while (true)
    {
        int randNaighborIndex = rand() % naighbors.count;
        MazeEdge *edge = naighbors[randNaighborIndex];
        if (edge.visited == NO)
        {
            nextEdge = edge;
            break;
        }
    }
    
}

- (NSArray *)naighborsFromEdge:(MazeEdge *)edge
{
    NSMutableArray *naighbors = [NSMutableArray array];
    CGSize mazeSize = self.mazeView.size;
    
    if (edge.object.row + 1 <= mazeSize.width)
        [naighbors addObject:[self.maze objectAtIndex:(edge.object.row + 1) * mazeSize.width + edge.object.column]];
    if (edge.object.row - 1 >= 0)
        [naighbors addObject:[self.maze objectAtIndex:(edge.object.row - 1) * mazeSize.width + edge.object.column]];
    if (edge.object.column - 1 >= 0)
        [naighbors addObject:[self.maze objectAtIndex:edge.object.row * mazeSize.width + edge.object.column - 1]];
    if (edge.object.column + 1 <= mazeSize.height)
        [naighbors addObject:[self.maze objectAtIndex:edge.object.row * mazeSize.width + edge.object.column + 1]];
    
    return naighbors;
}

- (void)makeAllWalls:(NSArray *)maze
{
    for (MatrixCell *cell in maze)
    {
        cell.value = 1;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.mazeView.size = CGSizeMake(10, 10);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.mazeView.size = CGSizeMake(10, 10);
}

@end
