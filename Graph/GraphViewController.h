//
//  GraphViewController.h
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatrixCell.h"

typedef enum
{
    GraphTypeSimple,
    GraphTypeShortestWay,
    GraphTypeColoring
} GraphType;

@interface GraphViewController : UIViewController

- (void)refreshGraphWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented;
- (void)matrixCellValueChanged:(MatrixCell *)cell;

- (void)findShortestWayWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented;
- (void)colorizeGraphWithMatrix:(NSArray *)matrix;

@end
