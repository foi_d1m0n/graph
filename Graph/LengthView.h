//
//  LengthView.h
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

static const int kLengthViewSize = 20;

@interface LengthView : UIView

@property (nonatomic) NSInteger lengh;

- (id)initWithCenterPoint:(CGPoint)center length:(NSInteger)length;

@end
