//
//  GraphView.h
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MatrixCell.h"

@interface GraphView : UIView

@property (nonatomic, strong) NSMutableArray *vertexes;
@property (nonatomic, strong) NSMutableArray *bonds;

@property (nonatomic) CGPoint innerCenter;

- (void)drawGraphWithMatrix:(NSArray *)matrix isOriented:(BOOL)isOriented;
- (void)drawNwewBondWithMAtrixCell:(MatrixCell *)cell;
- (void)deleteBondWithMatrixCell:(MatrixCell *)cell;

- (void)drawBondsLength;

- (void)clearGraph;

@end
