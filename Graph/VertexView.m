//
//  VertexView.m
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "VertexView.h"

@interface VertexView()

@property (nonatomic) NSInteger diamater;

@end

@implementation VertexView

- (id)initWithOrigin:(CGPoint)origin diamenter:(NSInteger)diamenter vertexNumber:(NSInteger)vertexNumber;
{
    self = [super initWithFrame:CGRectMake(origin.x - diamenter / 2, origin.y - diamenter / 2,
                                           diamenter, diamenter)];
    if (self)
    {
        self.vertexNumber = vertexNumber;
        self.diamater = diamenter;
        self.backgroundColor = [UIColor clearColor];
        self.bonds = [NSMutableArray array];
        self.color = [UIColor blueColor];
    }
    return self;
}

- (void)setColor:(UIColor *)color
{
    _color = color;
    [self setNeedsDisplay];
}

- (NSInteger)diameter
{
    return _diamater;
}

- (VertexView *)connectedVertexWithBond:(BondView *)bond
{
    if ([bond.firstVertex isEqual:self])
        return bond.secondVertex;
    else
        return bond.firstVertex;
}

- (BondView *)bondWhichConnectingVertexes:(NSArray *)vertexes
{
    VertexView *firstVertex = [vertexes objectAtIndex:0];
    VertexView *secondVertex = [vertexes lastObject];
    
    for (BondView *bond in firstVertex.bonds)
    {
        if (([bond.firstVertex isEqual:firstVertex] && [bond.secondVertex isEqual:secondVertex]) ||
            ([bond.firstVertex isEqual:secondVertex] && [bond.secondVertex isEqual:firstVertex]))
            return bond;
    }
    return nil;
}

- (void)addBond:(BondView *)bond
{
    [self.bonds addObject:bond];
}

- (void)deleteBond:(BondView *)bond
{
//    if (!bond.isDuplex)
//    {
        [self.bonds removeObject:bond];
//    }
}

- (BondView *)isBondedWithVertex:(VertexView *)vertex
{
    for (BondView *bond in self.bonds)
    {
        if ([bond.firstVertex isEqual:self])
        {
            if ([bond.secondVertex isEqual:vertex])
            {
                return bond;
            }
        }
        else
        {
            if ([bond.firstVertex isEqual:vertex])
            {
                return bond;
            }
        }
    }
    return nil;
}

- (NSArray *)connectedVertexes
{
    NSMutableArray *connectedVertexes = [NSMutableArray array];
    for (BondView *bond in self.bonds)
    {
        VertexView *connectedVertex = [self connectedVertexWithBond:bond];
        if (connectedVertexes)
            [connectedVertexes addObject:connectedVertex];
    }
    return connectedVertexes;
}

- (BOOL)isBondedWithVertex:(VertexView *)vertex andColor:(UIColor *)color
{
    for (BondView *bond in self.bonds)
    {
        if ([bond.firstVertex isEqual:self])
        {
            if ([bond.secondVertex isEqual:vertex] && bond.secondVertex.color == color)
            {
                return YES;
            }
        }
        else
        {
            if ([bond.firstVertex isEqual:vertex] && bond.secondVertex.color == color)
            {
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)isBondedWithChildVertexesWithColor:(UIColor *)color
{
    NSArray *connectedVertexes = [self connectedVertexes];
    BOOL connected = NO;
    for (VertexView *vertex in connectedVertexes)
    {
        if (vertex.color == color)
//        if ([self isBondedWithVertex:vertex andColor:color])
            connected = YES;
    }
    
    return connected;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(context, rect);
    CGContextSetFillColor(context, CGColorGetComponents([self.color CGColor]));
    CGContextFillPath(context);
    
    [self drawNumberInContext:context];
}

- (void)drawNumberInContext:(CGContextRef)context
{
    UIGraphicsPushContext(context);
    CGContextSetFillColor(context, CGColorGetComponents([[UIColor redColor] CGColor]));
    CGContextSetLineWidth(context, 3);
    NSString *valueString = [NSString stringWithFormat:@"%i", self.vertexNumber];
    [valueString drawAtPoint:CGPointMake(self.frame.size.width / 4,self.frame.size.height / 4) withFont:[UIFont boldSystemFontOfSize:25]];
    UIGraphicsPopContext();
}

@end
