//
//  MatrixListViewController.h
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MatrixListViewController;

@protocol MatrixListViewControllerDelegate <NSObject>

- (void)matrixListViewController:(MatrixListViewController *)matrixListViewController didSelectRecordWithName:(NSString *)name;

@end

@interface MatrixListViewController : UITableViewController

@property (nonatomic, weak) id <MatrixListViewControllerDelegate> delegate;

@property (nonatomic, strong) NSArray *recordNames;

@end
