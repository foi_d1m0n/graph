//
//  MatrixCell.m
//  Graph
//
//  Created by Dmitry Simkin on 3/23/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "MatrixCell.h"

@interface MatrixCell()

@end

@implementation MatrixCell

- (id)initWithRow:(NSInteger)row column:(NSInteger)column delegate:(id <MatrixCellDelegate>)delegate;
{
    self = [self init];
    if (self)
    {
        self.row = row;
        self.column = column;
        self.delegate = delegate;
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor yellowColor];
        self.value = 0;
        
        UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:tapGR];
        
        UILongPressGestureRecognizer *longPressGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
        [self addGestureRecognizer:longPressGR];
    }
    return self;
}

- (void)setValue:(NSInteger)value
{
    _value = value;
    [self setNeedsDisplay];
}

- (void)tapped:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        self.value += 1;

        if (self.value == 1)
            [self.delegate matrixCellDidDoubleTapped:self];
    }
}

- (void)longPressed:(UILongPressGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateRecognized)
    {
        self.value = 0;
            [self.delegate matrixCellDidDoubleTapped:self];
    }
}

- (void)drawRect:(CGRect)rect
{
    NSString *valueString = [NSString stringWithFormat:@"%i", self.value];
    [valueString drawAtPoint:CGPointMake(self.frame.size.width / 2,self.frame.size.height / 2) withFont:[UIFont systemFontOfSize:14.0]];
}

@end
