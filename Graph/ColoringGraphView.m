//
//  ColoringGraphView.m
//  Graph
//
//  Created by Dmitry Simkin on 05.04.13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "ColoringGraphView.h"

#import "VertexView.h"

@interface ColoringGraphView()

@property (nonatomic, strong) NSArray *colors;

@end

@implementation ColoringGraphView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor darkGrayColor];
        self.colors = @[[UIColor purpleColor],
        [UIColor orangeColor],
        [UIColor yellowColor],
        [UIColor brownColor],
        [UIColor cyanColor],
        [UIColor blueColor],
        [UIColor magentaColor]];
    }
    return self;
}

- (void)colorizeGraphWithMatrix:(NSArray *)matrix
{
    [super drawGraphWithMatrix:matrix isOriented:YES];
    
    [self colorizeGraph];
    
    [self setNeedsDisplay];
}

- (void)colorizeGraph
{
    NSMutableArray *uncolorizedVertexes = [self.vertexes mutableCopy];
    
//    VertexView *firstVertex = (VertexView *)[self.vertexes objectAtIndex:0];

    int i = 0;
    
    for (VertexView *vertex in self.vertexes)
    {
        if (!vertex.isColorized)
        {
            UIColor *currentColor = [self nextColorByIndex:i];
            vertex.color = currentColor;
            vertex.isColorized = YES;
            [uncolorizedVertexes removeObject:vertex];
            NSLog(@"vertex %i colorized to %i color", vertex.vertexNumber, i);
            NSMutableArray *currentColorizedVertexes = [NSMutableArray array];
            
            for (VertexView *otherVertex in uncolorizedVertexes)
            {
                if ([vertex isBondedWithVertex:otherVertex] || vertex == otherVertex)
                {
                    continue;
                }
                else
                {
                    if ([otherVertex isBondedWithChildVertexesWithColor:vertex.color])
                    {
                        continue;
                    }
                    else
                    {
                        otherVertex.color = currentColor;
                        NSLog(@"vertex %i colorized to %i color", otherVertex.vertexNumber, i);
                        otherVertex.isColorized = YES;
                        [currentColorizedVertexes addObject:otherVertex];
                    }
                }
            }
            
            [uncolorizedVertexes removeObjectsInArray:currentColorizedVertexes];
            i++;
        }
    }
}

- (UIColor *)nextColorByIndex:(int)index
{
    return [self.colors objectAtIndex:index];
}


@end
