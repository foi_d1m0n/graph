//
//  Module.m
//  Graph
//
//  Created by Dmitry Simkin on 4/7/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import "Module.h"

@implementation Module

- (id)initWithNumber:(NSInteger)number
{
    self = [super init];
    if (self)
    {
        self.number = number;
        self.functions = [NSMutableArray array];
    }
    return self;
}

- (int)functionsCount
{
    int count = 0;
    for (NSNumber *function in self.functions)
    {
        if (function.intValue)
            count++;
    }
    return count;
}

- (int)firstFunctionIndex
{
    for (int i = 0; i < self.functions.count; i++)
    {
        NSNumber *function = [self.functions objectAtIndex:i];
        if (function.intValue)
            return i;
    }
    return -1;
}

- (NSIndexSet *)functionIndexes
{
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for (int i = 0; i < self.functions.count; i++)
    {
        NSNumber *function = [self.functions objectAtIndex:i];
        if (function.intValue)
        {
            [indexSet addIndex:i];
        }
    }
    return indexSet;
}

- (void)removeFunctionAtIndexes:(NSIndexSet *)indexSet
{
//    NSArray *functionsToDelete = [self.functions objectsAtIndexes:indexSet];
//    
//    for (int i = 0; i < self.functions.count; i++)
//    {
//        NSNumber *function = [self.functions objectAtIndex:i];
//        if ([functionsToDelete containsObject:function] && function.intValue)
//        {
//            function = [NSNumber numberWithInt:0];
//        }
//    }
    NSMutableArray *replaceFunctions = [NSMutableArray array];
    for (int i = 0; i < indexSet.count; i++)
        [replaceFunctions addObject:[NSNumber numberWithInt:0]];
    
    [self.functions replaceObjectsAtIndexes:indexSet withObjects:replaceFunctions];
    
}

@end
