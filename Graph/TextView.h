//
//  NumberView.h
//  Graph
//
//  Created by Dmitry Simkin on 08.04.13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextView : UIView

@property (nonatomic, strong) NSString *text;

- (id)initWithFrame:(CGRect)frame text:(NSString *)text;

@end
