//
//  ShortestWayGraphView.h
//  Graph
//
//  Created by Dmitry Simkin on 3/24/13.
//  Copyright (c) 2013 Dmitry Simkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphView.h"

@interface ShortestWayGraphView : GraphView

@end
